﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DespensasAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DespensasAPI.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {

        }
        public DbSet<Candidato> Candidatos { get; set; }
        public DbSet<Entrevista> Entrevistas { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Asignado> Asignados { get; set; }
    }
}
