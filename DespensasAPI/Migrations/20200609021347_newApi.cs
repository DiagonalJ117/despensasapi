﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasAPI.Migrations
{
    public partial class newApi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Candidatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Telefono = table.Column<string>(nullable: true),
                    Calle = table.Column<string>(nullable: true),
                    NumeroExt = table.Column<string>(nullable: true),
                    Colonia = table.Column<string>(nullable: true),
                    Situacion = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    NombreReportante = table.Column<string>(nullable: true),
                    TelReportante = table.Column<string>(nullable: true),
                    UbicacionGMaps = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Candidatos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Empleados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Apellidos = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Entrevistas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CandidatoId = table.Column<int>(nullable: false),
                    EstatusLlamada = table.Column<string>(nullable: true),
                    FechaLlamada = table.Column<DateTime>(nullable: false),
                    NombreEntrevistador = table.Column<string>(nullable: true),
                    Notas = table.Column<string>(nullable: true),
                    NumPersonas = table.Column<int>(nullable: false),
                    NumMayores = table.Column<int>(nullable: false),
                    NumMenores = table.Column<int>(nullable: false),
                    ComidasAlDia = table.Column<int>(nullable: false),
                    ComidasMenores = table.Column<int>(nullable: false),
                    ComidasMayores = table.Column<int>(nullable: false),
                    SituacionActMenores = table.Column<string>(nullable: true),
                    SituacionActMayores = table.Column<string>(nullable: true),
                    NumPersonasAportan = table.Column<int>(nullable: false),
                    Dedicacion = table.Column<string>(nullable: true),
                    TipoEmpleo = table.Column<string>(nullable: true),
                    IngresoUltMes = table.Column<string>(nullable: true),
                    IngresoActual = table.Column<string>(nullable: true),
                    TotalGastosMes = table.Column<string>(nullable: true),
                    GastosMesPasado = table.Column<string>(nullable: true),
                    GastoAlimentos = table.Column<string>(nullable: true),
                    RecibeAyudaSocial = table.Column<bool>(nullable: false),
                    TipoAyudaSocial = table.Column<string>(nullable: true),
                    DeQuienRecibeAyuda = table.Column<string>(nullable: true),
                    FrecuenciaAyuda = table.Column<string>(nullable: true),
                    UltimaVezApoyo = table.Column<string>(nullable: true),
                    TiempoApoyo = table.Column<string>(nullable: true),
                    PersonasTerceraEdad = table.Column<int>(nullable: false),
                    PersonasCapacidades = table.Column<int>(nullable: false),
                    PersonasEnfCronicas = table.Column<int>(nullable: false),
                    TiempoMejorarSit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Entrevistas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Entrevistas_Candidatos_CandidatoId",
                        column: x => x.CandidatoId,
                        principalTable: "Candidatos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Asignados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: true),
                    CandidatoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Asignados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Asignados_Candidatos_CandidatoId",
                        column: x => x.CandidatoId,
                        principalTable: "Candidatos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Asignados_Empleados_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Asignados_CandidatoId",
                table: "Asignados",
                column: "CandidatoId");

            migrationBuilder.CreateIndex(
                name: "IX_Asignados_EmpleadoId",
                table: "Asignados",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Entrevistas_CandidatoId",
                table: "Entrevistas",
                column: "CandidatoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Asignados");

            migrationBuilder.DropTable(
                name: "Entrevistas");

            migrationBuilder.DropTable(
                name: "Empleados");

            migrationBuilder.DropTable(
                name: "Candidatos");
        }
    }
}
