﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasAPI.Models
{
    public class Asignado : Model
    {
        public int? EmpleadoId { get; set; }
        public virtual Empleado Empleado { get; set; }
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
    }
}
