﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Nombre { get; set; }
        public DateTime DateJoined { get; set; }
    }
}
