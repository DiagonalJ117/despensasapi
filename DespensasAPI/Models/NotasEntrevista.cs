﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasAPI.Models
{
    public class NotasEntrevista : Model
    {
        //1. ¿Cuántas personas viven en su domicilio?
        public int NumPersonas { get; set; }
        //2. De las personas que viven en su domicilio, ¿cuántas son mayores de 18 años?
        public int NumMayores { get; set; }
        //3. De las personas que viven en su domicilio, ¿cuántas son menores de 18 años?
        public int NumMenores { get; set; }
        //4. Durante su última semana, ¿cuántas veces al día está consumiendo alimento?
        public int ComidasAlDia { get; set; }
        //5. En la última semana, ¿la porción de alimento que consumen los menores de 18 años, corresponde a la que debe consumir para satisfacer su hambre?
        public int ComidasMenores { get; set; }
        //6. En la última semana, ¿la porción de alimento que consumen los mayores de 18 años, corresponde a lo que debe consumir para satisfacer su hambre?
        public int ComidasMayores { get; set; }
        //7. ¿La situación actual que menciona es diferente a la que tenía antes de COVID-19 para los menores de 18 años
        public string SituacionActMenores { get; set; }
        //8. ¿La situación actual que menciona es diferente a la que tenía antes de COVID-19 para los mayores de 18 años?:
        public string SituacionActMayores { get; set; }
        //9. ¿Cuántas personas aportan al ingreso familiar?
        public int NumPersonasAportan { get; set; }
        //10. ¿A qué se dedican las personas que aportan al ingreso familiar?
        public string Dedicacion { get; set; }
        //11. ¿Cuál es el tipo de empleo de la(s) persona(s) que aportan al ingreso familiar?
        public string TipoEmpleo { get; set; }
        //12. En el último mes, ¿cuál fue el ingreso total del hogar? Aquí se pueden apoyar un poco primero preguntando cuánto es su ingreso a la semana, diario según el tipo de empleo.
        public string IngresoUltMes { get; set; }
        //13. El ingreso actual que nos menciona ¿ha variado por COVID-19?
        public string IngresoActual { get; set; }
        //14. En el último mes, ¿cuál fue el total de gastos fijos del hogar? Aquí se pueden apoyar indagando cosas como: que es lo que tiene que pagar puntualmente para que no se vea afectado como servicios, créditos, prestamos, etc.
        public string TotalGastosMes { get; set; }
        //15. ¿El monto de gastos del mes pasado con respecto de este mes es...?
        public string GastosMesPasado { get; set; }
        //16. ¿Qué cantidad/porcentaje del ingreso total del hogar se destina a la compra de alimento? Aquí si pido su apoyo para que ustedes sean la que saquen el porentaje basandose en lo que les dijeron que tienen de ingreso con lo que tienen de gastos fijos.
        public string GastoAlimentos { get; set; }
        //17. ¿Actualmente en su hogar se recibe algún tipo de ayuda social?
        public bool RecibeAyudaSocial { get; set; }
        //18. ¿Qué tipo de ayuda social recibe en su hogar?
        public string TipoAyudaSocial { get; set; }
        //19. ¿De quién recibe la ayuda social?
        public string DeQuienRecibeAyuda { get; set; }
        //20 ¿Con qué frecuencia recibe la ayuda social?
        public string FrecuenciaAyuda { get; set; }
        //21. ¿Cuándo fue la última vez que recibió ese apoyo?
        public string UltimaVezApoyo { get; set; }
        //22. ¿Hace cuánto tiempo que recibe la ayuda social?
        public string TiempoApoyo { get; set; }
        //23. Actualmente, en su hogar ¿Habitan personas de la tercera edad?
        public int PersonasTerceraEdad { get; set; }
        //24. Actualmente, en su hogar ¿Habitan personas con capacidades diferentes que requieran en éste momento atención especializada?
        public int PersonasCapacidades { get; set; }
        //25. Actualmente, en su hogar ¿Habitan personas con alguna enfermedad crónica - degenerativa que requieran en éste momento atención especializada?
        public int PersonasEnfCronicas { get; set; }
        //26. ¿En cuánto tiempo cree usted que su situación va a mejorar?
        public string TiempoMejorarSit { get; set; }

    }
}
