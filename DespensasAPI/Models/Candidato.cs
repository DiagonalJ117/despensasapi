﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasAPI.Models
{
    public class Candidato : Model
    {
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Calle { get; set; }
        [DisplayName("Numero Exterior")]
        public string NumeroExt { get; set; }
        public string Colonia { get; set; }
        public string Situacion { get; set; }
        public string Descripcion { get; set; }
        public string NombreReportante { get; set; }
        [DisplayName("Telefono Reportante")]
        public string TelReportante { get; set; }
        [DisplayName("Ubicacion Google Maps")]
        public string UbicacionGMaps { get; set; }
    }
}
